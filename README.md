# pedro-germano-module-2

This is module 2 for the MTN-App academy for 2022

1. <h4>main.dart</h4> Write a basic program that stores and then prints the following data: Your name, favorite app, and city;
2. <h4>list.dart</h4> Create an array to store all the winning apps of the MTN Business App of the Year Awards since 2012; a) Sort and print the apps by name;  b) Print the winning app of 2017 and the winning app of 2018.; c) the Print total number of apps from the array.
3. <h4>data.dart</h4> Create a class and a) then use an object to print the name of the app, sector/category, developer, and the year it won MTN Business App of the Year Awards. b) Create a function inside the class, transform the app name to all capital letters and then print the output.

