void main() {
  var name = "Pedro";
  var favouriteApp = "facebook";
  var city = "São Paulo";

  print("My name is: $name");
  print("My favourite app to use is $favouriteApp");
  print("My city of choice is $city");
}
